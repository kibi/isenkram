=================
 isenkram-lookup
=================

-----------------------------------------------------------
look up hardware related packages relevant for this machine
-----------------------------------------------------------

:Author: The isenkram system is written Petter Reinholdtsen and others.  This manual page for isenkram-lookup was written by Petter Reinholdtsen.
:Date:   2017-09-01
:Copyright: GPL v2+
:Version: 0.0
:Manual section: 1
:Manual group: User Commands

SYNOPSIS
========

isenkram-lookup [modalias] ...

DESCRIPTION
===========

Extract all modalias values present in /proc on the current machine,
and check if these modalias values map to installable packages.

If arguments are given, these are interpreted as a modalias value and
used instead of the modalias values present on the current machine.

You can run this to locate all modalias strings for your machine:

  cat $(find /sys -name modalias) | sort -u

OPTIONS
=======

There are no options.

SEE ALSO
========

* isenkramd(1), isenkram-autoinstall-firmware(8), isenkram-pkginstall(8)
