# Copyright (C) 2013-2016 Petter Reinholdtsen <pere@hungry.com>
#
# Licensed under the GNU General Public License Version 2
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

from distutils.core import setup

isenkram = {}
with open('isenkram/__init__.py', 'r') as version_file:
    exec (version_file.read(), isenkram)

setup(name='Isenkram',
      version     = isenkram['__version__'],
      description = isenkram['__summary__'],
      author      = isenkram['__author__'].split('<')[0].strip(),
      author_email= isenkram['__author__'].split('<')[1].split('>')[0].strip(),
      url         = isenkram['__homepage__'],
      packages    = ['isenkram', ],
      license     = isenkram['__license__'],
      )
