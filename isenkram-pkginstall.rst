=====================
 isenkram-pkginstall
=====================

------------------------------------------------------------------------------
locate and install hardware specific packages suitable for the current machine
------------------------------------------------------------------------------

:Author: Petter Reinholdtsen - pere@hungry.com
:Date:   2017-09-01
:Copyright: GPL v2+
:Version: 0.0
:Manual section: 8
:Manual group: User Commands

SYNOPSIS
========

isenkram-pkginstall

DESCRIPTION
===========

Look up the hardware present in the current machine and look up
packages that would be useful for this hardware in various sources for
mapping hardware to packages.

OPTIONS
=======

There are no options.

SEE ALSO
========

* isenkram-lookup(1), isenkramd(1), isenkram-autoinstall-firmware(8)
