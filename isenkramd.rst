===========
 isenkramd
===========

--------------------------------------------------------------
user desktop background process to handle new hardware dongles
--------------------------------------------------------------

:Author: Petter Reinholdtsen - pere@hungry.com
:Date:   2017-09-01
:Copyright: GPL v2+
:Version: 0.0
:Manual section: 1
:Manual group: User Commands

SYNOPSIS
========

isenkramd

DESCRIPTION
===========

This background desktop process is started by the desktop autostart
mechanism and listen for udev events to handle new hardware dongles.
When a new dongle is detected, sources for mapping hardware to
packages are consulted and a GUI notification is shown to the user to
suggest to install the hardware relevant packages.

OPTIONS
=======

There are no options.

SEE ALSO
========

* isenkram-autoinstall-firmware(8), isenkram-pkginstall(8)
